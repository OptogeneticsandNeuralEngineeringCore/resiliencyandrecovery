I"�%<h1 id="1-alternatives-to-teensy">1 Alternatives to Teensy</h1>

<p>We built the dynamic clamp system around the Teensy 3.6 microcontroller because, compared to alternatives of its class and price range, it is faster and has more memory. However, it is important to note that other microcontrollers could be used in its place. In particular, microcontrollers that can be programmed using the Arduino integrated development environment (IDE) require only small modifications to the code. There are many microcontrollers like this because Arduino has emerged as a standard in the community.</p>

<p>To demonstrate this point, we describe in this document how to use an Arduino Due or a chipKit uC32 in place of the Teensy 3.6. Neither is the equal of the Teensy 3.6 in speed or memory, but both are plausible alternatives.  The code for both alternatives is archived with the rest of the software for this project at the public repository Github (<a href="https://github.com/nsdesai/dynamic_clamp">https://github.com/nsdesai/dynamic_clamp</a>).</p>

<h3 id="a-arduino-due">A. Arduino Due</h3>

<p>The Arduino line of microcontrollers includes many products (<a href="https://www.arduino.cc">https://www.arduino.cc</a>). At this writing (08/26/17), the product best suited to the dynamic clamp technique is the Arduino Due (clock speed 84 MHz, 512 kB flash memory, 92 kB SRAM memory). It includes 12 ADC inputs and 2 DAC outputs.</p>

<p>In the folder <em>dynamic_clamp_arduino_due</em>, we have modified the dynamic clamp code to work with the Arduino Due.</p>

<p><em>Main file</em>. The main file differs from that of the Teensy main file in only two ways. (1) The pin connections have different names. The first DAC output of the Due is called “DAC0” whereas that of the Teensy is “A21”. The EPSC trigger pin we use for the Due is number 13 rather than number 2. (2) To keep track of time, we use the built-in Arduino functions <em>millis()</em> and <em>micros()</em> rather than the data types <em>elapsedMillis</em> and <em>elapsedMicros</em>. The latter types are also available for the Due but using them requires installing a separate library. Of course, this is simple to do but we chose instead to illustrate how to use <em>millis()</em> and <em>micros()</em> in place of the elapsed time data types; both methods work with the Teensy 3.6.</p>

<p><em>Tabbed (conductance) files</em>. The Teensy has a floating point unit (FPU). To instruct the compiler to use the FPU, one uses special functions that end in the letter f in place of standard functions. One uses <em>expf()</em> to calculate an exponential rather than <em>exp()</em>; one uses <em>sinf()</em> to calculate the sine rather than <em>sin()</em>. The Due does not have an FPU. To make the code compatible with the Due, we simply removed the letter f – that is, we replaced the special FPU functions with the standard functions.</p>

<p>One other thing to keep in mind when using the Due: although the DAC outputs are nominally 3.3 V, they do not cover the full range between 0 V and 3.3 V. Instead they only cover the range 0.55 V to 2.75 V. The output calibration parameters (slope and intercept) will therefore be different from what one would have calculated using a Teensy.</p>

<h3 id="b-chipkit-uc32">B. chipKit uC32</h3>

<p>chipKit is a line of microcontrollers based on Microchip Technology’s PIC32 microcontroller chip (<a href="https://chipkit.net">https://chipkit.net</a>). Even though it is not an Arduino product, it can still be programmed using the Arduino IDE.</p>

<p>One nice thing about the chipKit uC32 is that there is an add-on called Analog Shield (<a href="http://bit.ly/2wehEpp">http://bit.ly/2wehEpp</a>) available for it. This add-on allows the uC32 to read and write voltages between -5 V and +5 V directly. This means that users will not need to build the differential amplifier parts of the system (parts 2 and 4 of Fig. 1B of the main text). Of course, this convenience comes at a price ($50 as of this writing).</p>

<p>In the folder <em>dynamic_clamp_chipKit_uC32</em>, we have modified the dynamic clamp code to work with the chipKit uC32.</p>

<p>Main file. In addition to the two changes that apply to the Arduino Due (pin connections with different names; using <em>millis()</em> and <em>micros()</em> instead of the elapsed time data types), the uC32 main file must be modified in other respects to take advantage of the Analog Shield. (1) At the top of the file, we include two necessary libraries (<em>analogShield.h</em> and <em>SPI.h</em>). (2) The functions <em>analogRead()</em> and <em>analogWrite()</em> are replaced by <em>analog.read()</em> and <em>analog.write()</em>. (3) The ADC inputs and DAC outputs of the Analog Shield are 16 bit rather than 12 bit. So the calibration numbers are all different from those for the Teensy and the output is constrained to be less than 65536 (= 2^16) rather than less than 4096 (= 2^12).</p>

<p><em>Tabbed (conductance) files</em>. As in the Arduino Due case, we must remove the letter f because the uC32 lacks an FPU.</p>

<h1 id="2-matlab-alternative-to-processing">2 Matlab Alternative to Processing</h1>

<p>The code for this alternative is archived with the rest of the software for this project at the public repository Github (<a href="https://github.com/nsdesai/dynamic_clamp">https://github.com/nsdesai/dynamic_clamp</a>).</p>

<p>We used the open-source environment Processing (<a href="www.processing.org">www.processing.org</a>) to control the Teensy microcontroller. Processing is useful because it is free and platform independent. However, many alternatives exist and these may be especially attractive to researchers who use other programs to acquire patch clamp data. All that is required is that the program can send a list of numbers to the microcontroller through the USB port.</p>

<p>One possibility is Matlab, which has an extensive user base in the neuroscience community and for which free electrophysiology software is available (e.g., <a href="wavesurfer.janelia.org">wavesurfer.janelia.org</a> or <a href="clm.utexas.edu/robotpatch">clm.utexas.edu/robotpatch</a>).</p>

<p>In our example software, the Teensy microcontroller waits to receive eight numbers through the USB port. The eight numbers represent the values of (1) shunt conductance (<em>g_shunt</em> in nS), (2) HCN conductance (<em>g_HCN</em> in nS), (3) sodium conductance (<em>g_Na</em> in nS), (4) excitatory Ornstein-Uhlenbeck (OU) mean conductance (<em>m_OU_exc</em> in nS), (5) excitatory OU diffusion constant (<em>D_OU_exc</em> in nS2/ms),  (6) inhibitory Ornstein-Uhlenbeck (OU) mean conductance (<em>m_OU_inh</em> in nS), (7) inhibitory OU diffusion constant (<em>D_OU_inh</em> in nS2/ms), and (8) EPSC conductance (<em>g_EPSC</em> in nS). When the microcontroller receives these numbers, it changes the value of the conductances and diffusion constants appropriately.</p>

<p>Each number must be a byte array representation of single precision float point (4 bytes containing 32 bits). What this means is described here (<a href="https://en.wikipedia.org/wiki/Single-precision_floating-point_format">https://en.wikipedia.org/wiki/Single-precision_floating-point_format</a>). This topic can be somewhat arcane, but fortunately most languages have built-in methods to convert numbers from one type to another. For example, in Matlab, we can convert the number pi into a byte array using the function typecast like this:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>&gt;&gt;  pi = 3.141592653589793;            % 64-bit representation

&gt;&gt;  pi_single_precision = single(pi)  % 32-bit representation

pi_single_precision =3.1415927

&gt;&gt; pi_byte_array = typecast(pi_single_precision,'uint8')    % byte array representation

pi_byte_array = 219   15   73   64
</code></pre></div></div>

<p>One can also go the other way: to convert the byte array back into a (32-bit) floating point, enter this line at the command prompt: typecast(pi_byte_array, ’single’).</p>

<p>In the Matlab script Matlab_simple_example.m, we show how to write eight conductance/constant numbers to the Teensy. Before using this script, users should change the serial port name (e.g., ‘COM3’) to the name of the USB port to which their Teensy is attached.</p>

<p>We also provide a Matlab graphical user interface (GUI) with which users can control the board. It has the same functionality as the Processing GUI. To use it, (1) save the two files (<em>DC.m</em> and <em>DC.fig</em>) in the Matlab current directory; (2) open <em>DC.m</em> and find the line “initializeteensy(‘COM3’)”; (3) change COM3 to the name of the USB port for your set-up; (4) save the file; and (5) open the GUI by typing “DC” at the command line. This will open up the GUI, which looks like this:</p>

<p><img src="/resiliencyandrecovery/assets/img/matlab.png" alt="lazy.jpg" title="lazy.png" /></p>

<p>The sliders can be moved to select new values for the conductances and constants. Pressing Upload sends these values to the Teensy microcontroller. Pressing Zero zeroes all the values and sends zeros to the microcontroller (i.e., so that no dynamic clamp conductances are being simulated).</p>

<hr />

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen=""></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>

:ET