---
layout: page-fullwidth
title: "Hints"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/hints/"
---
This pages contains some more miscellaneous information that you might find useful.

# A. The complete circuit schematic

![lazy.jpg](/resiliencyandrecovery/assets/img/complete_circuit.png "lazy.png")

# B. The completed breadboard

![lazy.jpg](/resiliencyandrecovery/assets/img/14_full_ciruit.jpg "lazy.png")

![lazy.jpg](/resiliencyandrecovery/assets/img/15_full_circuit.jpg "lazy.png")

# C. Hints from grad students

Three grad students in our lab worked together to make individual dynamic clamps for themselves.  Here are some hints from them.

# D. Brief construction directions

1. Order the parts
2. Determine the input and output gains you need for your amplifier and choose the proper resistors
3. First build the power supply and test it to make sure you have +9 and -9V
4. Build the circuit enumerated as “2” on the Methods page or Fig. 2B in the paper.  Don’t connect the output to the Teensy yet.
5. Test it. You can use a signal generator, or the output of your amplifier to generate an input voltage.  Hook up a model cell on your amplifier and set the current injection or holding potential so the output gives you a voltage in the range of  -8V to +8V. Or you can direct your system data-acquisition system to output a specified voltage. Measure the output of that circuit in response to several values of the input voltage.  Here’s what mine looks like:

    ![lazy.jpg](/resiliencyandrecovery/assets/img/circuitB.png "lazy.png")

6. Now build the circuit for part “B” on the Methods page, or Figure 2D in the paper.  Don’t connect it to the Teensy yet.  Test it using a range of input voltages, measuring the output.  I found it useful to replot the graph switching the axes and scaling them to the DAC output and proper amplitude for the amplifier input. Here’s mine:

    ![lazy.jpg](/resiliencyandrecovery/assets/img/circuitD-701x1024.png "lazy.png")

7. Build the circuit for part “5” on the methods page, or Figure 2E in the paper.  Test it with  several input voltages.  This circuit has 2 inputs (it’s a summing circuit).  Test with each one individually, and with the same signal going to both.  Here’s mine; it’s not perfect.  The slope with both inputs connected should be 2, but 1.9 is pretty close.

    ![lazy.jpg](/resiliencyandrecovery/assets/img/circuitE.png "lazy.png")


8. When you have the individual parts of the circuit working, you can connect them together and proceed to calibration.  My method of measuring outputs with various inputs gives you the slopes and intercepts you need, but it’s a good idea to follow the calibration instructions and make sure they’re right.

9. That’s it! less than 10 steps.

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
