---
layout: page-fullwidth
title: "Parts"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/parts/"
---

| Note | Part Name                                                        | Vendor            | Part Number      | QTY | Price | Link                                                                                                                                                |
|------|------------------------------------------------------------------|-------------------|------------------|-----|-------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
|      |                                                                  |                   |                  |     |       |                                                                                                                                                     |
| A    | Teensy 3.6 (microcontroller)                                     | sparkfun.com      | DEV-14058        | 1   | 34    | [https://www.sparkfun.com/products/14058](https://www.sparkfun.com/products/14058)                                                                                                             |
| B    | Op-Amp (LM358)                                                   | sparkfun.com      | COM-09456        | 3   | 1     | [https://www.sparkfun.com/products/9456](https://www.sparkfun.com/products/9456)                                                                                                              |
| C    | Resistor kit                                                     | amazon.com        | Elegoo 17 Values | 1   | 10    | [https://www.amazon.com/Elegoo-Values-Resistor-Assortment-Ohm-1M/dp/B072BL2VX1/ref=sr_1_2?ie=UTF8&qid=1504304005&sr=8-2&keywords=elegoo+resistor+kit](https://www.amazon.com/Elegoo-Values-Resistor-Assortment-Ohm-1M/dp/B072BL2VX1/ref=sr_1_2?ie=UTF8&qid=1504304005&sr=8-2&keywords=elegoo+resistor+kit) |
| D    | Capacitor kit                                                    | sparkfun.com      | KIT-13698        | 1   | 7     | [https://www.sparkfun.com/products/13698](https://www.sparkfun.com/products/13698)                                                                                                             |
| E    | Breadboard (solderless)                                          | adafruit.com      | 239              | 1   | 6     | [https://www.adafruit.com/product/239](https://www.adafruit.com/product/239)                                                                                                                |
| F    | DC barrel jack adapter                                           | sparkfun.com      | PRT-10811        | 1   | 1     | [https://www.sparkfun.com/products/10811](https://www.sparkfun.com/products/10811)                                                                                                             |
| G    | Jumper wire kit                                                  | sparkfun.com      | PRT-00124        | 1   | 7     | [https://www.sparkfun.com/products/124](https://www.sparkfun.com/products/124)                                                                                                               |
| H    | 18V (+) DC wall adapter                                          | amazon.com        | Dunlop ECB004    | 1   | 15    | [https://www.amazon.com/gp/product/B0002E3FBK/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B0002E3FBK/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)                                                            |
| I    | BNC male to screw terminal connector                             | parts-express.com | 090-106          | 4   | 1     | [https://www.parts-express.com/bnc-male-to-screw-terminal-connector--090-106?utm_source=google&utm_medium=cpc&utm_campaign=pla](https://www.parts-express.com/bnc-male-to-screw-terminal-connector--090-106?utm_source=google&utm_medium=cpc&utm_campaign=pla)                       |
| I'   | BNC female to screw terminal connector (some prefer this gender) | parts-express.com | 090-108          | 4   | 1     | [https://www.parts-express.com/bnc-female-to-screw-terminal-connector--090-108](https://www.parts-express.com/bnc-female-to-screw-terminal-connector--090-108)                                                                       |
| J    | TLE 2426 rail splitter IC                                        | mouser.com        | 595-TLE2426IP    | 1   | 2     | [http://www.mouser.com/Search/ProductDetail.aspx?R=TLE2426IPvirtualkey59500000virtualkey595-TLE2426IP](http://www.mouser.com/Search/ProductDetail.aspx?R=TLE2426IPvirtualkey59500000virtualkey595-TLE2426IP)                                                |
| K    | USB Micro B cable                                                | sparkfun.com      | CAB-13244        | 1   | 2     | [https://www.sparkfun.com/products/13244](https://www.sparkfun.com/products/13244 ) 

***
NOTES

A       Teensy 3.6 can be purchased both with and without header pins already attached. Buy the one with pins.

B       Each LM358 contains two separate op-amps. Other op-amps would also work, but make sure you buy enough: you’ll need at least five.

C       This is a resistor assortment pack. It’s convenient if a bit wasteful. These are fairly ordinary resistors; substituting resistors you happen to have in your lab will probably work fine.

D       Same comment as for C, but with the word “capacitor” substituted for “resistor.”

E       This is a full-size solderless breadboard. You can also put the system together on a solder-able breadboard or a perf board.

F       This adapter has pins that fit snugly into the breadboard, but you can use any kind of barrel adapter.

G       An assortment pack of wire to connect things. You can use 22 AWG solid core wire instead.

H       This particular adapter has a positive outer shell and a negative inner pin. Other wall adapters have the opposite polarity. Make sure you know which you have before connecting anything else.

I       The connectors offer a simple, inexpensive way to connect the system to the BNC ports of typical DAQ boards and patch clamp amplifiers.

J       This is a standard and widely-used rail splitter IC.

K       This cable connects the Teensy 3.6, which has a 5-pin Micro  B USB input, to the USB port of a computer.

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
