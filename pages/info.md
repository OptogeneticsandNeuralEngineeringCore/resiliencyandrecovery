---
layout: redirect
title: "About"
subheadline: "Why another Jekyll Theme?"
teaser: "Since years I am programming and designing websites. I love to work with open source tools and learn via code from others. This time I want to try to give something back..."
permalink: "/info/"
redirect_to: "/resiliencyandrecovery/"
---


 [1]: http://mademistakes.com/about/
 [2]: http://mademistakes.com/work/jekyll-themes/
 [3]: http://automattic.com/
 [4]: http://alistapart.com/
 [5]: http://www.smashingmagazine.com/
 [6]: https://github.com/
 [7]: http://sauer.io
 [8]: {{ site.url }}/design/gallery/
 [9]: {{ site.url }}/design/video/
 [10]: {{ site.url }}/design/grid/
 [11]: {{ site.url }}/design/typography/
 [12]: {{ site.url }}/design/mediaelement_js/
 [13]: #
 [14]: #
 [15]: #
 [16]: #
 [17]: #
 [18]: #
 [19]: #
 [20]: #
