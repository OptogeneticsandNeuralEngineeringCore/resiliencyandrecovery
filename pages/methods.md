---
layout: page-fullwidth
title: "Methods"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/methods/"
---

<b style='color:red;'>NOTE: This is the methods section from the paper (Desai, Gray, and Johnston. A dynamic clamp on every rig. eNeuro 4(5) ENEURO.0250-17.2017  (2017): [LINK](http://www.eneuro.org/content/4/5/ENEURO.0250-17.2017)). It is reproduced here for the convenience of the reader. It is advisable to read the entire paper carefully before starting to assemble the system.</b>

***

A schematic overview of the system is

![lazy.jpg](/resiliencyandrecovery/assets/img/fig1a.png "lazy.png")

The portions in black are present on every intracellular electrophysiology rig: an amplifier and a data acquisition (DAQ) board. The amplifier could be a Multiclamp 700B, a Dagan BVC-700A, a HEKA EPC-10, an AM Systems 2400, or any other amplifier that monitors a neuron’s membrane potential and injects current into that neuron through a patch or sharp electrode. The DAQ board could be a Molecular Devices Digidata 1500, a HEKA ITC-18, a National Instruments PCIe-6343, or any of a huge number of other boards that work with Macintosh, Windows, or Linux operating systems; our dynamic clamp system places no constraint in this regard. We assume that the DAQ board is controlled on the host computer by a DAQ system suitable for intracellular electrophysiology. The system could be one of the commercial systems on the market (e.g., Molecular Devices pClamp 10 or AxoGraph) or it could be open source (e.g., Janelia’s Wavesurfer) or it could be homemade. The point is simply that the electrophysiology rig already includes the components necessary to patch or impale neurons and record current clamp data.

Into this existing, working configuration, we insert the portion depicted in red below. It consists of a Teensy 3.6 microcontroller and associated electronics. We chose the microcontroller because, compared to other devices of its class and in its price range, it is fast (180 MHz clock speed), has substantial memory (256 kB RAM), and has a floating point unit (more on this in the Discussion). The Teensy is responsible in our system for performing all the dynamic clamp calculations. It determines, moment by moment, what current a voltage- or ligand-gated conductance would pass were it physically present and adds this to the current that the existing DAQ system has been instructed to inject (e.g., a family of current steps). That is, the existing DAQ system continues to perform all the standard current clamp (or voltage clamp) functions. The Teensy system simply adds a dynamic clamp component – it adds the current from simulated conductances to the current that the existing current clamp system specifies.

![lazy.jpg](/resiliencyandrecovery/assets/img/fig1b-1024x339.png "lazy.png")

For the Teensy to do this, some electronic additions are required: (1) a power supply, (2) circuitry to map the voltage output of the intracellular amplifier (typically ±9 V) representing the neuron’s membrane potential to the voltages the Teensy can read (0-3.3 V), (3) electrical connections to and from the Teensy, (4) circuitry to transform the Teensy’s output (0-3.3 V) into a voltage the intracellular amplifier can correctly interpret (typically ±9 V) as a current (in pA) to be injected into the neuron, and (5) circuitry to sum the dynamic clamp currents specified by the Teensy and the current clamp currents specified by the DAQ system. We explain these five additions, all of which can be built on a single solderless breadboard (Figure 1C), in the five sections that immediately follow.

![lazy.jpg](/resiliencyandrecovery/assets/img/fig1c-1024x420.png "lazy.png")

After that, we discuss the software that controls the Teensy. While the Teensy 3.6 is not a microcontroller of the Arduino line (www.arduino.cc), it is very similar and can be programmed using the (open source) Arduino integrated development environment (IDE). We provide, in the online material, the code we used to program various simulated conductances; these serve as examples for users who might wish to program different simulated conductances. We also discuss code written in the (open source) Processing language (www.processing.org) to change dynamic clamp parameters “on the fly” (i.e., during a  recording). Processing is a useful language because it is very simple and because, like Arduino, its code can be used without modification on all three major operating systems (Macintosh, Windows, Linux).

We end the methods section by discussing how to calibrate the electronic components of the dynamic clamp system to ensure best performance.

The parts list, photographs, and step-by-step instructions can be accessed by the tabs at the top of the webpage. Post-publication updates will be available at this website ([dynamicclamp.com](https://dynamicclamp.com/)), with software archived at the public repository Github ([https://github.com/nsdesai/dynamic_clamp](https://github.com/nsdesai/dynamic_clamp)).

***

## Power supply

 The power supply serves two purposes: it provides power for the operational amplifiers (“op-amps”) of the other circuits and it provides the positive (negative) reference voltages the other circuits use to shift up (down) the voltages sent to (from) the Teensy microcontroller. For both these purposes, we require a positive voltage (approximately +9 V) and a negative voltage (approximately -9 V).

 The simplest power source suitable for both purposes is an 18 V DC wall adaptor (colloquially called a “wall wart”). Such an adaptor typically terminates in a barrel plug that can be plugged into a barrel connector. (We provide a full parts list, including links to supplier webpages, under the Parts tab, so that readers can see for themselves what all the parts look like.) This power source cannot work alone because it is positive only, whereas we wish to have both positive and negative voltages. More precisely, we wish to break up the +18 V into one rail at +9 V, one rail at -9 V, with a ground (called a “virtual ground”) right at the halfway point.

 Circuits that perform this function are called “rail splitters.” In principle, a purely passive rail splitter circuit – essentially a voltage divider – would suffice here, but in this design we opted for an op-amp circuit, because it minimizes asymmetry between the positive and negative rails and buffers the power supply from the downstream circuits.  (Keeping one part of the system from interfering with other parts – “buffering” – is a general principle of electronic design.) The op-amp circuit we chose is a ubiquitous integrated circuit (IC) from Texas Instruments (TLE2426). This shows schematically how the IC is connected.

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2a-1024x705.png "lazy.png")

Under the Construction tab, we describe and show (with pictures) how the circuit looks when the parts are physically connected on a solderless breadboard. The breadboard has four power rails. One rail is marked +9 V and another is marked -9 V. The downstream circuits use these two rails to power their own op-amps and as reference points for +9 V and -9 V. A third rail on the breadboard is connected to the virtual ground of the rail splitter. Every other voltage in this system will be referenced to this ground rail.

***

## Amplifier output to microcontroller input

An intracellular electrophysiology amplifier in current clamp mode monitors a neuron’s membrane potential and outputs a signal representing this value. For real neurons, the membrane potential will be in a range no wider than -90 mV to +90 mV. The representative output will depend on the gain of the amplifier, but for the typical settings of commonly-used amplifiers the corresponding range will be -9 V to +9 V. This is too broad a range for the analog inputs of the Teensy microcontroller (or other controllers of this class), which are limited to 0 to 3.3 V.

To map ±9 V from the amplifier onto 0-3.3 V to the microcontroller, we employ three distinct elements. The first is a *follower* circuit. It takes an input (±9 V) and simply sends out an identical output (±9 V). The follower’s purpose is to separate – buffer – the input from the output, to keep them from interfering with each other. The second is a *voltage divider* that transforms the voltage from the amplifier (±9 V) onto a more limited range (approximately ±1.6 mV, the precise numbers depend on the precise resistor values chosen). The third is a *differential amplifier*f that adds (approximately) 1.6 V to shift the output of the second element into the range 0-3.2 V, which roughly matches the dynamic range of the Teensy’s analog-to-digital (ADC) input.

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2b-1024x496.png "lazy.png")

One can calculate the relationship between the input (VIN) to this three-element circuit and its output (VOUT) by assuming that all the resistor values and power supply voltages are exact and that all the op-amps are ideal (Senturia and Wedlock, 1975). This is it:

![lazy.jpg](/resiliencyandrecovery/assets/img/eqn1-1-1024x95.png "lazy.png")

In this equation , V+ is the voltage of the positive power rail (+9 V). Note that the relationship between  *Vin* and *Vout* is linear. We constructed this circuit on a breadboard using resistor values between 100 Ω and 22 kΩ (specified in the figure caption) and an IC that contains two op-amps (LM358n). Testing the breadboard circuit, we found that the empirical relationship between *Vin* and *Vout* was indeed strictly linear (see “Calibration” section below), but that the numerical values of the slope and intercept were somewhat different from what the exact equation would predict (by ~2%). This discrepancy resulted from imperfections in the (inexpensive) electronic components we chose and the non-ideal behavior of the op-amps of the LM358n chip.

Fortunately, the discrepancy can be corrected in software, without having to substitute better (and more expensive) electronic components. As explained in the “Calibration” section, this can be done by measuring *Vout* values in response to a range of known *Vin* values. The numbers can be fitted to a straight line and the resulting slope and intercept used instead of the calculated slope and intercept.


## Microcontroller connections

The output of the three-element circuit (now 0-3.2 V) is fed to an ADC input on the Teensy microcontroller (Fig. 2C). The Teensy 3.6 has 25 ADC inputs; our default software simply selects the first of these (A0, pin 14), but any can be used. The Teensy analog ground should be connected to the virtual ground defined by the rail splitter circuit. The Teensy has two digital-to-analog (DAC) outputs; our default software uses the first of these (A21).

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2c-1024x637.png "lazy.png")

## Microcontroller output to amplifier input

The output of the Teensy DAC will be a voltage between 0 V and 3.3 V, but most  amplifiers in current-clamp mode expect command voltages between -9 V and +9 V, with negative voltages representing hyperpolarizing current injections and positive voltages representing depolarizing current injections. Mapping 0-3.3 V onto the range ±9 V is the inverse of the problem we faced earlier, and its solution is similar but inverted. We use a differential amplifier both to shift 0-3.3 V down to the range ±1.65 V and to amplify the result (Fig. 2D).

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2d-1024x794.png "lazy.png")

Assuming perfect, ideal components, we can calculate the relationship between the input supplied by the Teensy (Vdac) and the output of the circuit (Vdc, representing the dynamic clamp command signal) (Senturia and Wedlock, 1975):

![lazy.jpg](/resiliencyandrecovery/assets/img/eqn2-1-1024x113.png "lazy.png")

Here,  V– is the voltage of the negative power rail (-9 V). Again, the relationship between input (Vdac) and output (Vdc) is linear. And again, when we constructed the circuit on a breadboard, we found that, while the empirical relationship between *Vdac* and *Vdc* was linear, it was not strictly given by the calculated formula (off by ~2%). This discrepancy too can be resolved in software (see “Calibration” section).

## Summing Circuit

The fifth and last electronic circuit is designed to sum the dynamic clamp command voltage *Vdc* and the current clamp command voltage from the DAQ system Vdac. Summing voltages is a common electronics task, and we perform it in a standard fashion: an inverting amplifier that sums the two voltages but switches their polarity followed by a second inverting amplifier that switches the polarity back.

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2e-1024x500.png "lazy.png")

## Software

#### Arduino IDE

There are multiple ways of programming the Teensy 3.6, including simply using the C language, but the most sensible way to do so is through the Arduino IDE [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software). Arduino has emerged over the last five years as the microcontroller of choice of the maker movement, including nearly all the neuroscience-related projects (Baden et al., 2015; Desai et al., 2015; Potter et al., 2014; Siegle et al., 2017). The Arduino IDE and its associated language retain the essential syntax of C while making the process of interfacing with a microcontroller straightforward. Although Teensy is not part of the Arduino line of microcontrollers, a Teensy-specific add-on to the Arduino IDE exists and allows one to use the IDE and most of its libraries [https://www.pjrc.com/teensy/teensyduino.html](https://www.pjrc.com/teensy/teensyduino.html). Detailed installation and use instructions are included under the Software tab.

We wrote our dynamic clamp software using the Arduino IDE. The code is contained in the Extended Data folder dynamic_clamp and the main file is called dynamic_clamp.ino. Opening the latter not only opens the main file but its associated files, which appear in separate tabs. Each tabbed file contains the code for a specific conductance. The main file is structured in three parts: global variables, a set-up function, and a loop function. The global variables are self-explanatory (variables needed by all and therefore accessible to all functions); the set-up function is run once when the program is uploaded to the board and does things like initialize serial communication between the host computer and the Teensy board; the loop function is run at every time step – it calls each of the conductance-specific tabbed files to get the current specified by that conductance.

In the example software, we coded five separate conductances: (1) a simple shunt conductance, (2) a hyperpolarization-activated cyclic nucleotide–gated (HCN) conductance (Gasparini et al., 2004), (3) a fast, transient sodium conductance (Johnston and Wu, 1994), (4) an excitatory postsynaptic conductance (EPSC) (Compte et al., 2000), and (5) “high conductance state” synaptic background conductances (Destexhe et al. 2001, 2003). The first is simple. The second and third are Hodgkin-Huxley conductances with one and four gates, respectively. The fourth is a synaptic conductance that is triggered by a TTL pulse sent by the DAQ board to the Teensy microcontroller. The fifth is comprised by two conductances – one excitatory, one inhibitory – generated by Ornstein-Uhlenbeck (OU) processes. Our example code demonstrates how to numerically integrate the stochastic OU equations and how to generate the Gaussian random numbers the OU processes require (Marsaglia and Bray, 1964). Together these five examples span the range of conductances users are likely to wish to employ, and this code is meant to provide templates from which users can create other conductances.

As a further aid, in the Extended Data, we also describe step-by-step how to add a potassium M conductance (Fransen et al., 2002).

#### Processing

When the Arduino program is uploaded to the Teensy microcontroller, all of the dynamic clamp conductances are initialized to zero. They can be changed to non-zero values while the program is running. That is, the dynamic clamp conductances can be changed “on the fly” during a given recording.

The simplest way for the host computer to tell the microcontroller to modify the dynamic clamp conductances is through the USB port that connects them. In our default Arduino software, the microcontroller constantly checks for a serial communication from the host computer, and changes the conductance values as soon as it arrives. Unfortunately, the Arduino IDE itself has no good way of sending real-time communication from the host computer to the microcontroller. Fortunately, many other programs do. One called Processing (www.processing.org) is especially well suited for this purpose: it is an open-source environment with a simple syntax (based on Java and thus possessing a family resemblance to the C-like Arduino) that has a huge user base and is platform independent (Windows, Macintosh, Linux).

In the Extended Data, we include a Processing sketch (called *processing_control.pde*) that creates a graphical user interface (GUI) through which users can change the values of the maximal conductances (in nS) of the five conductances of our default software. The GUI also allows users to modify the diffusion constants (nS2/ms) of the OU processes. All the parameters can be modified by moving the sliders in the GUI and pressing the “upload” button.

But there is nothing unique about Processing. Users are free to use any software they wish in order, for example, to couple their data acquisition and dynamic clamp software more tightly. To emphasize this point, we also include Matlab software (Windows, Macintosh, Linux) in the Extended Data that does the same things as the Processing sketch.

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
