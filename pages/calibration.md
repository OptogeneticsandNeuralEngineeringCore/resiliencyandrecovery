---
layout: page-fullwidth
title: "Calibration Procedure"
#meta_title: "Feeling Responsive Theme Changelog"
#subheadline: "Feeling Responsive Theme Changelog"
#teaser: "History and changelog of Feeling Responsive Theme"
#header:
#   image_fullwidth: "header_unsplash_9.jpg"
permalink: "/calibration/"
---

#### Know how to use a breadboard

Resistor and capacitor values are inexact and operational amplifiers do not behave ideally. Therefore the slope and intercept values calculated in the main text for the circuits of Figure 2B (which maps the amplifier output, ±9 V, onto a range the Teensy can read, 0-3.3 V) and Figure 2D (which maps the Teensy output, 0-3.3V, onto a range the amplifier command input expects, ±9 V) will not be quite correct. They will be close (within a few percent), but we can do better by running the following calibration procedure.

***

## 1. Expected values

First, let us do the exact calculations as given in the main text so that we know (roughly) what numbers to expect. If the numbers we get after calibration are very different from these numbers (>5-10% error), that is an indication that something is wrong — e.g., the wrong resistors are on the breadboard — and we should check all the components.

*Relating Vm to the ADC input of the Teensy*

In the main text, we noted that the voltage input to the circuit of Figure 2B is related to the voltage output like this:

![lazy.jpg](/resiliencyandrecovery/assets/img/Eq1-1.png "lazy.png")

The voltage V+ is the voltage of the positive power rail (+9 V).   (Note well: the equation above assumes that R2 << R3, except when the voltage divider is omitted altogether, i.e. R1 = 0 and R2 = ∞.)  With the resistor values R1=2200 Ω, R2=470 Ω, R3=4700 Ω, R4=22000 Ω, R5=10000 Ω, and R6=100 Ω, this equation becomes:

![lazy.jpg](/resiliencyandrecovery/assets/img/Eq2.png "lazy.png")

Now, *Vout* is not read directly by the Teensy microcontroller. Rather, for a given voltage, the ADC input to the microcontroller equals (4096/3.3)*voltage. That is, the ADC input maps voltages from 0 to 3.3 V onto the range 0 to 4095. (Note: 12 bits = 2^12 = 4096. There are 4096 distinct integer levels between 0 and 4095.) So, in terms of the ADC input, the equation is:

![lazy.jpg](/resiliencyandrecovery/assets/img/Eq3.png "lazy.png")

Inverting this equation, we have:

![lazy.jpg](/resiliencyandrecovery/assets/img/Eq4.png "lazy.png")

*Vin* is the voltage (in volts) sent out by the patch clamp amplifier. If the amplifier is set so that its input gain is gainINPUT (the number of millivolts sent out for every millivolt of membrane potential read in), then the relationship between the ADC reading and the membrane potential Vm (in mV) is this:

![lazy.jpg](/resiliencyandrecovery/assets/img/Picture1-2.png "lazy.png")

Note that we merely replaced *Vin* with Vm*gainINPUT/1000 to get this equation.

*Relating I to the DAC output of the Teensy*

The dynamic clamp simulation specifies what current *IDC*  (in pA) should be injected into the neuron, but the signal to the DAC output must be an integer between 0 and 4095 (12 bits) representing voltages between 0 and 3.3 V. We have to map one representation onto the other.

The amplifier receives a voltage command and interprets this number as a current depending on its output gain, gainoutput
(number of pA sent out for every V read in — for example, this would be 400 pA/V for a Multiclamp 700B with a 500 MΩ feedback resistor):

![lazy.jpg](/resiliencyandrecovery/assets/img/Eq6-300x48.png "lazy.png")

where *IDC*  and *VDC* are the dynamic clamp current and command voltage, respectively. They are defined in Figs. 1B and 2D. As we noted in the main text, the command voltage is related to the voltage from the Teensy, VDAC, like this:

![lazy.jpg](/resiliencyandrecovery/assets/img/Picture1-1.png "lazy.png")

The voltage *V–* is the voltage of the negative power rail (-9 V). Combining the two equations and using the resistor values R7=4700 Ω, R8=22000 Ω, R9=10000 Ω, and R10=10000 Ω, we have the following relationship between current *IDC*  (in pA) and the DAC voltage output *VDAC* (in V):

![lazy.jpg](/resiliencyandrecovery/assets/img/Picture1-3.png "lazy.png")

*VDAC* (in V) is related to the DAC output (0-4095) in this way: *VDAC*  = (3.3/4096) * DAC .  Finally, we have the relationship between the current the dynamic clamp system specifies and the DAC output of the Teensy:

![lazy.jpg](/resiliencyandrecovery/assets/img/Picture2-3.png "lazy.png")

*Putting the calibration numbers into the Arduino program*

We can use the two boxed equations to add calibration numbers to the main file of the Arduino program. On our system (Multiclamp 700B, 500 MΩ feedback resistor, with the current clamp gain set at 5), the input gain is 50 mV/mV and the output gain is 400 pA/V. So, we will fill out the calibration section of the main file in this way:

![lazy.jpg](/resiliencyandrecovery/assets/img/Capture.png "lazy.png")

## 2. Calibration program

Even with high quality components, the calibration numbers we have just calculated will be slightly off. Therefore, it is better to measure the input and output of the dynamic clamp system directly and use those numbers instead.



*Direct method*

UPDATE: We plan to make instructional videos to show people how to calibrate and use the system. We might even make a video showing how to build it. At the moment I’ve only made two (kind of crummy) videos illustrating the “direct” method of calibration. You can find them at this Box link: calibration_videos . Hopefully they will soon be replaced by better videos.

The most direct way of calibrating the system exactly is to use one analog input and one analog output of your DAQ system (e.g., National Instruments board, ITC-18, or Digidata). You can also substitute other ways of measuring voltages (e.g., a multimeter or oscilloscope) and/or sending out voltages (e.g., a signal generator).



Calibrating the input circuit
![lazy.jpg](/resiliencyandrecovery/assets/img/fig2b-1024x496.png "lazy.png")

Connect your DAQ system’s analog output to *Vin* and the analog input to *Vout* . Make sure the 18 V adapter is plugged in.

Send a range of voltages (between -7 V and +7 V) to VIN, and measure the resulting values of VOUT. Convert the values of *Vout* into the ADC numbers (0-4095) the Teensy’s analog input reads: ADC = *Vout* * 4096 / 3.3. The relationship between the *Vin* and ADC should be strongly linear (as in main text Fig. 3A). By fitting a straight line you can extract the inputSlope and inputIntercept numbers, as we did above. Remember to account for gain_INPUT and the factor of 1000 that enters in because *Vin* is typically in volts whereas Vm is typically in millivolts.

Calibrating the output circuits

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2d-1024x794.png "lazy.png")

![lazy.jpg](/resiliencyandrecovery/assets/img/fig2e-1024x500.png "lazy.png")

To get the output slope and intercept, upload the Arduino sketch *specifyoutput.ino* (contained in the *calibration* folder of the Github repository) to the Teensy microcontroller. This time connect your DAQ system’s analog input (or your digital multimeter’s input) so that it measures *VAMP* of main text Fig. 2E (measured against virtual ground). Connect VDAQ directly to ground with a wire. You can send the Teensy a number between 0-4095 over the USB port and it will output that number from its DAC port. To do this, open Arduino’s Serial Monitor and type in the number. Do this for a broad range of numbers between 0 and 4095, recording the output (VAMP) for each. Again, the relationship between the DAC numbers and *VAMP*  will be strongly linear (similar to Fig. 3A of main text). Convert *VAMP* into *IDC* , using *IDC*  = *gainoutput* * VAMP, and fit a straight line with *IDC*  as the x variable and the DAC numbers as the y variable. The slope and intercept give the outputSlope and outputIntercept of the main Arduino program (note that, in the case of outputSlope, you have to divide out the value of *gainoutput* you used when doing the calibration).

These calibration numbers inputSlope, inputIntercept, outputSlope, and outputIntercept should not differ from the calculated values by any more than 10%. Comparing the two sets of numbers is a good way to make sure the circuit on the board was assembled correctly. NOTE WELL: In the Arduino sketch, we separate out the gainINPUT and *gainoutput* contributions to inputSlope, inputIntercept, and outputSlope. We do this to make a clear distinction between the part of the calibration numbers that depend on the amplifier settings and those that depend on the breadboard circuits. So, when inserting numbers into the Arduino sketch, make sure you separate out gainINPUT and *gainoutput* as we did in the first section of this calibration document.



Model cell method

A less direct but simpler method is to attach a model cell to the amplifier’s headstage and use the Arduino program (*teensy_calibration.ino*) and the Processing program (*processing_calibration.pde*) provided at the Github site.

In general the equivalent circuit of the model cell will look something like this:

![lazy.jpg](/resiliencyandrecovery/assets/img/circuit1-300x216.png "lazy.png")

This is the Patch-1U model cell (“cell mode”) from Molecular Devices. When subjected to a constant current I0, it should exhibit a steady-state voltage deflection of ∆*VSS* = *I0* * 510 MΩ. (Note that this depends only on the resistors. In the steady state, the capacitors have dropped out.) In practice, because of imperfections in the components that make up the model cell (see, e.g., http://bit.ly/2qHavi1) the total resistance will be somewhat different from 510 MΩ. But this can be calculated directly – just as one would calculate input resistance during a whole cell recording. You should do so for your own model cell.



(1) Having gotten this number, put it in the Processing sketch:

![lazy.jpg](/resiliencyandrecovery/assets/img/processing1.png "lazy.png")

(2) Enter the name of the USB port to which the Teensy microcontroller is attached in the Processing program in its *setup()* function. The default is “COM3”.

![lazy.jpg](/resiliencyandrecovery/assets/img/processing2.png "lazy.png")

(3) Upload the Arduino program to the Teensy microcontroller.

(4) Run the Processing program by pressing the right-facing arrow at the upper left of the Processing window. This will open up the following GUI:

![lazy.jpg](/resiliencyandrecovery/assets/img/Capture-1.png "lazy.png")

The top half concerns calibration of the Teensy’s input (how it relates a given ADC reading with a membrane potential Vm ). The bottom half concern’s calibration of the Teensy’s output (how it relates a desired dynamic clamp current *IDC*  with an output signal to send to the Teensy’s DAC output). In other words, the top half calculates the values of inputSlope and inputIntercept, and the bottom half calculates the values of outputSlope and outputIntercept.

First, we run the top half of the GUI, thus fixing inputSlope and inputIntercept. Then we run the bottom half, fixing outputSlope and outputIntercept. The order matters: we need the Teensy to be able to take a good measurement of *VM* in order to get the output parameters right.

(5) Change your patch clamp amplifier to I-clamp mode and connect your DAQ system’s analog output directly to the command input of your amplifier, bypassing the summing circuit of Fig. 2E. That is, instead of connecting the DAQ system’s analog output to VDAQ, connect it to the amplifier directly as you would normally do.

(6) At this point, with no current being injected by the amplifier or the dynamic clamp system, zero the amplifier Vm reading to 0 mV. Use the slider on the Processing GUI to set the V_m to 0 mV as well. Press the measure button. Processing has now read a value from the Teensy’s ADC input and associated it with Vm = 0 mV.

(7) Adjust the value of the amplifier’s measurement of Vm to some new value by injecting some holding current. With a Multiclamp 700A or 700B, you can just do this from the Multiclamp commander. With other amplifiers, you may need to use your DAQ system to specify the holding current. Use the Processing slider to set V_m equal to this new value of Vm, and press the *measure* button. Repeat this procedure approximately 10 times, for values of Vm between -80 mV and +50 mV.  If you make a mistake and want to start over, press *zero*; this clears all the saved values.

(7) Press the *fit* button. Processing will fit a straight line using the saved values of amplifier Vm and the microcontroller ADC input. The slope and intercept will be displayed in the GUI window. You can use these to set the values of inputSlope and inputIntercept in the main Arduino dynamic clamp program, as we did at the end of the *Expected values* section above. Remember to adjust the values based on the gainINPUT parameters of your amplifier.

Pressing  *fit* also saves the values used in the fitting to a text file saved to the same folder as the processing_calibration.pde file. It will have a name like Vm_YYYY_MM_DD_hh_mm_ss.txt, where YYYY is the year, MM is the month, DD is the day, hh is the hour, and ss is the second. The first column will be the Vm readings, the second column will be the ADC readings. The two columns will be separated by commas and the numbers can be loaded, for example, into Excel in case you wish to check Processing’s calculations.

(8) Having established the inputSlope and inputIntercept numbers, we now can establish the outputSlope and outputIntercept numbers. Disconnect the DAQ system’s analog input from the amplifier’s command input, and connect it to VDAQ of the dynamic clamp board. Connect the dynamic clamp board’s output *VAMP* to the amplifier’s command input.

(9) Switch the amplifier to I-clamp mode and make sure the amplifier is not injecting any current (i.e., “holding current” is equal to 0). Press the current button at the bottom of the Processing GUI. Teensy will now set its DAC output to a range of values between 0 and 4095 while recording membrane potential. By diving the latter numbers by the model cell’s resistance, it will convert them into current readings (in pA). It will then fit a straight line and display the values of outputSlope and outputIntercept. Use these to fill out the calibration section of the main Arduino dynamic clamp program, as we did at the end of the Expected values section above. Again, be mindful to use the value of *gainoutput* specific to your amplifier.

The numbers used in the fitting will be saved to a text file in the same folder as the processing_calibration.pde file. It will have a name like I_YYYY_MM_DD_hh_mm_ss.txt. The first column will be the DAC output, the second column will be the current. They will be separated by commas and the numbers can be imported into Excel and many other programs, so that you can check that there are no problems.

Before ending, it is important to emphasize the importance of comparing any measured calibration numbers (whether direct method or model cell method) to the expected numbers of the exact calculations (section I). While resistors and op-amps are not ideal, even the cheapest ones are not really that bad. Well-calibrated numbers should differ from the calculated ones by (much) less than 10%. Any discrepancy larger than this should cause you to double check all the connections and use a multimeter (or the equivalent) to check each component.

***

# ACKNOWLEDGMENTS

This section was considerably improved by comments and suggestions from Andrew Scallon of the Optogenetics and Neural Engineering (ONE) Core facility at the University of Colorado Anschultz Medical Campus (https://optogeneticsandneuralengineeringcore.github.io/ONECoreSite/).


 <figure>
   <img src="/resiliencyandrecovery/assets/img/SuppFig16_19.png" alt="my img" class="center"/>
   <figcaption> Supplementary Figure 16. The voltages (VIN, VDAQ, VAMP, and TTL) are the same as those of main text Fig. 2. The letters (A, B, C, D, E) refer to the five panels of main text Fig 2.</figcaption>
 </figure>

 ***

# ACKNOWLEDGEMENTS

This section was considerably improved by comments and suggestions from Andrew Scallon of the Optogenetics and Neural Engineering (ONE) Core facility at the University of Colorado Anschultz Medical Campus (https://optogeneticsandneuralengineeringcore.github.io/ONECoreSite/).

***

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>


 [1]: {{ site.url }}/blog/
 [2]: {{ site.url }}/blog/archive/
 [3]: http://foundation.zurb.com/docs/components/accordion.html
 [4]: {{ site.url }}/design/gallery/
 [5]: {{ site.url }}/design/video/
 [6]: https://www.google.de/maps/place/Strandpaviljoen+Joep+B.V./@51.9960733,5.830135,6z/data=!4m2!3m1!1s0x47cf5918df69093b:0x7c11ab31102c1c8a
 [7]: fontcustom.com
 [8]: https://www.tawk.to
 [9]: https://github.com/jjamor
 [10]: #
