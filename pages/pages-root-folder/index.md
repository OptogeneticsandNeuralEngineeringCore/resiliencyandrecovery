---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: page-fullwidth
#header:
#  image_fullwidth: header_unsplash_12.jpg


# Necessary for index to generate, probably because homepage: true below...?
widget1:
  title: "Blog & Portfolio"
  url: 'http://phlow.github.io/feeling-responsive/blog/'
  image: widget-1-302x182.jpg
  text: '.'

#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#

# Necessary for index to generate, probably because homepage: true below...?
callforaction:
  url: https://tinyletter.com/feeling-responsive
  text: Inform me about new updates and features ›
  style: alert
permalink: /index.html

#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---

# Welcome

An open-minded, all inclusive campus group welcoming all Anschutz Medical Campus students, PRAs, and postdocs.  

Resiliency & Recovery focuses on creating a safe discussion space for individuals quietly struggling with mental health, and / or substance use. Though our campus offers plentiful resources to its students and employees, individuals may be hesitant, for a myriad of reasons, to accept said resources, or to begin a formal process of treatment. Thus, it is likely that a sub-population of our campus group is failing to receive support which may greatly benefit them. 

With these considerations in mind, Resiliency & Recovery offers an easily accessible, and non-formal stepping stone to personal recovery. This may present itself in various forms; those in dire need of additional resources will be directed to them, if requested. Similarly, and to no less importance, those having a bad day or a bad week, can come by for a cup of coffee and easy discussion with peers.

Resiliency & Recovery follows no traditional meeting structure, and each Friday meeting will typically begin utilizing a starter topic, and will follow-up with discussion amongst the group, in which participants can freely discuss what’s on their minds. 

Resiliency & Recovery requires absolutely no registration, no sign-in, includes no faculty presence, and requests that participants refrain from discussing the meeting and its participants outside of the meeting room. In all, these are our best efforts to respect the anonymity of our group members. 

Please contact [resiliencyandrecovery@gmail.com](emailto:resiliencyandrecovery@gmail.com) for additional information, and we hope to see you there!

![NAna.jpg](/resiliencyandrecovery/assets/img/3.png "NAna.png")

-R&R Leadership
